// Generated by CoffeeScript 1.3.3
(function() {

  window.mlh = (function() {
    var eventize, handlers, raise_event;
    handlers = {};
    eventize = function(func, eventName) {
      func.bind = function(callback) {
        if (typeof handlers[eventName] === 'undefined') {
          handlers[eventName] = [];
        }
        return handlers[eventName].push(callback);
      };
      return func.unbind = function(callback) {
        var index;
        if (typeof handlers[eventName] !== 'undefined') {
          if (typeof callback === 'undefined') {
            return handlers[eventName] = [];
          } else {
            index = handlers[eventName].indexOf(callback);
            if (index >= 0) {
              return handlers[eventName].splice(index, 1);
            }
          }
        }
      };
    };
    raise_event = function(eventName, args) {
      var handler, _i, _len, _ref, _results;
      if (typeof handlers[eventName] !== 'undefined') {
        _ref = handlers[eventName];
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          handler = _ref[_i];
          _results.push(handler.apply(window, args));
        }
        return _results;
      }
    };
    window.mdm_disable = function() {
      return raise_event('disable', arguments);
    };
    eventize(window.mdm_disable, "disable");
    window.mdm_enable = function() {
      return raise_event('enable', arguments);
    };
    eventize(window.mdm_enable, 'enable');
    window.mdm_prompt = function(message) {
      return raise_event('prompt', arguments);
    };
    eventize(window.mdm_prompt, 'prompt');
    window.mdm_noecho = function(message) {
      return raise_event('noecho', arguments);
    };
    eventize(window.mdm_noecho, 'noecho');
    window.mdm_msg = function(message) {
      return raise_event('msg', arguments);
    };
    eventize(window.mdm_msg, 'msg');
    window.mdm_timed = function(message) {
      return raise_event('timed', arguments);
    };
    eventize(window.mdm_timed, 'timed');
    window.mdm_error = function(message) {
      return raise_event('error', arguments);
    };
    eventize(window.mdm_error, 'error');
    window.mdm_add_user = function(username, gecos, status) {
      return raise_event('add_user', arguments);
    };
    eventize(window.mdm_add_user, 'add_user');
    window.mdm_add_session = function(session_name, session_file) {
      return raise_event('session', arguments);
    };
    eventize(window.mdm_add_session, 'add_session');
    window.mdm_add_language = function(language_name, language_code) {
      return raise_event('add_language', arguments);
    };
    eventize(window.mdm_add_language, 'add_language');
    window.mdm_set_current_language = function(language_name, language_code) {
      return raise_event('set_current_language', arguments);
    };
    eventize(window.mdm_set_current_language, 'set_current_language');
    window.mdm_hide_shutdown = function() {
      return raise_event('hide_shutdown', arguments);
    };
    eventize(window.mdm_hide_shutdown, 'hide_shutdown');
    window.mdm_hide_suspend = function() {
      return raise_event('hide_suspend', arguments);
    };
    eventize(window.mdm_hide_suspend, 'hide_suspend');
    window.mdm_hide_restart = function() {
      return raise_event('restart', arguments);
    };
    eventize(window.mdm_hide_restart, 'hide_restart');
    window.mdm_hide_quit = function() {
      return raise_event('hide_quit', arguments);
    };
    eventize(window.mdm_hide_quit, 'hide_quit');
    window.mdm_hide_xdmcp = function() {
      return raise_event('hide_xdmcp', arguments);
    };
    eventize(window.mdm_hide_xdmcp, 'hide_xdmcp');
    return {
      login: function(password) {
        return alert("LOGIN###" + password);
      },
      set_user: function(username) {
        return alert("USER###" + username);
      },
      set_sesion: function(sessionName, sessionFile) {
        return alert("SESSION###" + sessionName + "###" + sessionFile);
      },
      set_language: function(languageCode) {
        return alert("LANGUAGE###" + languageCode);
      },
      shutdown: function() {
        return alert('SHUTDOWN###');
      },
      suspend: function() {
        return alert('SUSPEND###');
      },
      restart: function() {
        return alert('RESTART###');
      },
      quit: function() {
        return alert('QUIT###');
      },
      set_xdmcp: function() {
        return alert('XDMCP###');
      },
      on_disable: mdm_disable,
      on_enable: mdm_enable,
      on_prompt: mdm_prompt,
      on_noecho: mdm_noecho,
      on_msg: mdm_msg,
      on_timed: mdm_timed,
      on_error: mdm_error,
      on_add_user: mdm_add_user,
      on_add_session: mdm_add_session,
      on_add_language: mdm_add_language,
      on_set_current_language: mdm_set_current_language,
      on_hide_shutdown: mdm_hide_shutdown,
      on_hide_suspend: mdm_hide_suspend,
      on_hide_restart: mdm_hide_restart,
      on_hide_quit: mdm_hide_quit,
      on_hide_xdmcp: mdm_hide_xdmcp
    };
  })();

}).call(this);
