# Joel Kuhn 2013
# joelakuhn@gmail.com
#
# All the functions handled here came from the very handy bootstrap theme
# that is currently bundled with MDM.

window.mlh = (->

  handlers = {}

  eventize = (func, eventName) ->
    func.bind = (callback) ->
      if typeof handlers[eventName] == 'undefined'
        handlers[eventName] = []
      handlers[eventName].push(callback)

    func.unbind = (callback) ->
      if typeof handlers[eventName] != 'undefined'
        if typeof callback == 'undefined'
          handlers[eventName] = []
        else
          index = handlers[eventName].indexOf(callback)
          handlers[eventName].splice(index, 1) if index >= 0

  raise_event = (eventName, args) ->
    if typeof handlers[eventName] != 'undefined'
      for handler in handlers[eventName]
        handler.apply(window, args)


  # Called by MDM to disable user input
  window.mdm_disable = ->
    raise_event('disable', arguments)
  eventize(window.mdm_disable, "disable")

  # Called by MDM to enable user input
  window.mdm_enable = ->
    raise_event('enable', arguments)
  eventize(window.mdm_enable, 'enable')

  # Called by MDM to allow the user to input a username
  window.mdm_prompt = (message) ->
    raise_event('prompt', arguments)
  eventize(window.mdm_prompt, 'prompt')

  # Called by MDM to allow the user to input a password
  window.mdm_noecho = (message) ->
    raise_event('noecho', arguments)
  eventize(window.mdm_noecho, 'noecho')

  # Called by MDM to show a message (usually "Please enter your username")
  window.mdm_msg = (message) ->
    raise_event('msg', arguments)
  eventize(window.mdm_msg, 'msg')

  # Called by MDM to show a timed login countdown
  window.mdm_timed = (message) ->
    raise_event('timed', arguments)
  eventize(window.mdm_timed, 'timed')

  # Called by MDM to show an error
  window.mdm_error = (message) ->
    raise_event('error', arguments)
  eventize(window.mdm_error, 'error')

  # Called by MDM to add a user to the list of users
  window.mdm_add_user = (username, gecos, status) ->
    raise_event('add_user', arguments)
  eventize(window.mdm_add_user, 'add_user')

  # Called by MDM to add a session to the list of sessions
  window.mdm_add_session = (session_name, session_file) ->
    raise_event('session', arguments)
  eventize(window.mdm_add_session, 'add_session')

  # Called by MDM to add a language to the list of languages
  window.mdm_add_language = (language_name, language_code) ->
    raise_event('add_language', arguments)
  eventize(window.mdm_add_language, 'add_language')

  # Called by MDM to set the current language so that it can be selected
  window.mdm_set_current_language = (language_name, language_code) ->
    raise_event('set_current_language', arguments)
  eventize(window.mdm_set_current_language, 'set_current_language')

  # Called by MDM if the SHUTDOWN command shouldn't appear in the greeter
  window.mdm_hide_shutdown = ->
    raise_event('hide_shutdown', arguments)
  eventize(window.mdm_hide_shutdown, 'hide_shutdown')

  # Called by MDM if the SUSPEND command shouldn't appear in the greeter
  window.mdm_hide_suspend = ->
    raise_event('hide_suspend', arguments)
  eventize(window.mdm_hide_suspend, 'hide_suspend')

  # Called by MDM if the RESTART command shouldn't appear in the greeter
  window.mdm_hide_restart = ->
    raise_event('restart', arguments)
  eventize(window.mdm_hide_restart, 'hide_restart')

  # Called by MDM if the QUIT command shouldn't appear in the greeter
  window.mdm_hide_quit = ->
    raise_event('hide_quit', arguments)
  eventize(window.mdm_hide_quit, 'hide_quit')

  # Called by MDM if the XDMCP command shouldn't appear in the greeter
  window.mdm_hide_xdmcp = ->
    raise_event('hide_xdmcp', arguments)
  eventize(window.mdm_hide_xdmcp, 'hide_xdmcp')

  return {
    login: (password) ->
      alert("LOGIN####{password}")

    ,set_user: (username) ->
      alert("USER####{username}")

    ,set_sesion: (sessionName, sessionFile) ->
      alert("SESSION####{sessionName}####{sessionFile}")

    ,set_language: (languageCode) ->
      alert("LANGUAGE####{languageCode}")

    ,shutdown: ->
      alert('SHUTDOWN###')

    ,suspend: ->
      alert('SUSPEND###')

    ,restart: ->
      alert('RESTART###')

    ,quit: ->
      alert('QUIT###')

    ,set_xdmcp: ->
      alert('XDMCP###')

    ,on_disable: mdm_disable
    ,on_enable: mdm_enable
    ,on_prompt: mdm_prompt
    ,on_noecho: mdm_noecho
    ,on_msg: mdm_msg
    ,on_timed: mdm_timed
    ,on_error: mdm_error
    ,on_add_user: mdm_add_user
    ,on_add_session: mdm_add_session
    ,on_add_language: mdm_add_language
    ,on_set_current_language: mdm_set_current_language
    ,on_hide_shutdown: mdm_hide_shutdown
    ,on_hide_suspend: mdm_hide_suspend
    ,on_hide_restart: mdm_hide_restart
    ,on_hide_quit: mdm_hide_quit
    ,on_hide_xdmcp: mdm_hide_xdmcp
  }
)()
