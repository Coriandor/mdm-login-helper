# mdm-login-helper.js

mdm-login-helper.js (heretofore MLH) is a small wrapper around the functionality of the new html5 login page feature of MDM.

## Usage

### Events

First of all, everything in MLH lives in a modlue called *mlh*. MDM relies on several functions such as *mdm\_add\_user* and *mdm\_hide\_shutdown* to live in the global scope in order to manage behavior on the login page. Having just one function declared in global scope for each "event" is a bit clumsy, so MLH aliases these functions in a the mlh module and adds an event system to them.

To bind a callback to the mdm\_add\_user event, simple do:

```Javascript
mlh.on_add_user.bind(function(username, gecos, status) {
    // add the user
});
```

To unbind a callback:

```Javascript
mlh.on_add_user.unbind(callback);
```

To unbind all callbacks from an event:

```Javascript
mlh.on_add_user.unbind();
```

Events can also be triggered by calling the event function:

```Javascript
mlh.on_disable();

mlh.on_set_language(sessionName, languageCode);
```

The events that are currently handled along with their signatures are: 

```Javascript
mlh.on_disable()
mlh.on_enable()
mlh.on_prompt(message)
mlh.on_noecho(message)
mlh.on_msg(message)
mlh.on_timed(message)
mlh.on_error(message)
mlh.on_add_user(username, gecos, status)
mlh.on_add_session(session_name, session_file)
mlh.on_add_language(language_name, language_code)
mlh.on_set_current_language(language_name, language_code)
mlh.on_hide_shutdown()
mlh.on_hide_suspend()
mlh.on_hide_restart()
mlh.on_hide_quit()
mlh.on_hide_xdmcp()
```

### Actions

Actions sent from the login page to MDM are sent via specially formated strings passed to the alert function. Since that's a pretty silly way to do things, MLH wraps these actions into functions.

The currently supported actions are:

```Javascript
mlh.login(password)
mlh.set_user(username)
mlh.set_language(languageCode)
mlh.set_session(sessionName, sessionFile)
mlh.shutdown()
mlh.restart()
mlh.suspend()
mlh.quit()
mlh.set_xdmcp()
```
